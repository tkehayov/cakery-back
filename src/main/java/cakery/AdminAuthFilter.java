package cakery;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

public class AdminAuthFilter implements Filter {
	private static final String username = "ubata";
	private static final String password = "uba";

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		System.out.println("hello");
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;

		if (!isLogged(request)) {
			String reqUser = req.getParameter("username");
			String reqPass = req.getParameter("password");
			if (reqUser != null && reqPass != null) {
				if (reqUser.equals(username) && reqPass.equals(password)) {
					response.addCookie(new Cookie("username", username));
					response.addCookie(new Cookie("password", password));
					response.sendRedirect("/admin/indexAdmin.html#/admin");
					return;
				}
			}
			
			response.sendRedirect("/login.html");
			return;
		}

		chain.doFilter(req, res);
	}

	@Override
	public void destroy() {
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {

	}

	private boolean isLogged(HttpServletRequest request) {
		Cookie[] cookies = request.getCookies();

		boolean isLogged = false;
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				String cookieName = cookie.getName();
				if (cookieName.equals("username")) {
					isLogged = cookie.getValue().equals(username) ? true : false;
					continue;
				}

				if (cookieName.equals("password")) {
					isLogged = cookie.getValue().equals(password) ? true : false;
				}
			}
		}
		return isLogged;
	}
}
