package cakery.rest;

import cakery.Constant;
import cakery.core.ImageNameGenerator;
import cakery.core.StorageService;
import cakery.core.reciept.Reciept;
import cakery.core.reciept.RecieptRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static cakery.core.reciept.Reciept.newBuilder;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
public class RecieptController {
    @Autowired
    private RecieptRepository recieptRepository;

    private StorageService storageService;
    private ImageNameGenerator imageGenerator;

    @Autowired
    public RecieptController(StorageService storageService, ImageNameGenerator imageGenerator) {
        this.storageService = storageService;
        this.imageGenerator = imageGenerator;
    }

    public RecieptController() {

    }

    @RequestMapping(value = "/reciepts", method = POST)
    public void add(@RequestBody RecieptDTO dto) {
        recieptRepository.add(toDo(dto));
    }

    @RequestMapping(value = "/reciepts", method = GET)
    public List<RecieptListDTO> list(@RequestParam(value = "limit", defaultValue = "10") int limit) {
        List<Reciept> list = recieptRepository.list(limit);

        return toDTO(list);
    }

    @RequestMapping(value = "/reciepts/{category}/categories/", method = GET)
    public List<RecieptListDTO> categories(@PathVariable String category) {
        List<Reciept> list = recieptRepository.category(category);

        return toDTO(list);
    }

    @RequestMapping(value = "/{id}/reciepts", method = GET)
    public RecieptDTO get(@PathVariable String id) {
        Reciept reciept = recieptRepository.get(id);

        return toDTO(reciept);
    }

    @RequestMapping(value = "/reciepts/{id}", method = DELETE)
    public void delete(@PathVariable String id) {
        recieptRepository.delete(id);
    }

    @RequestMapping(value = "/reciepts/image", method = RequestMethod.POST, produces = "text/plain; charset=utf-8")
    public String handleFileUpload(@RequestParam("file") MultipartFile file) {
        String randomImageNameGenerator = null;
        try {
            File dir = getDir();
            randomImageNameGenerator = imageGenerator.generate(file.getOriginalFilename());
            storageService.save(file.getInputStream(), dir + "/" + randomImageNameGenerator);
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        return randomImageNameGenerator;
    }

    private Reciept toDo(RecieptDTO dto) {
        return newBuilder()
                .title(dto.title)
                .content(dto.content)
                .cookingTime(dto.cookingTime)
                .category(dto.category)
                .cookingTimeAll(dto.cookingTimeAll)
                .cookingPreperationTime(dto.cookingPreperationTime)
                .directions(dto.directions)
                .ingredients(dto.ingredients)
                .createdDate(dto.createdDate)
                .frontImageGallery(dto.frontImageGallery)
                .shortDescription(dto.shortDescription)
                .frontImage(dto.frontImage)
                .images(dto.images)
                .build();
    }

    private List<RecieptListDTO> toDTO(List<Reciept> reciepts) {
        return reciepts.stream()
                .map(r -> RecieptListDTO.newBuilder()
                        .title(r.title)
                        .cookingTime(r.cookingTime)
                        .id(r.id)
                        .shortDescription(r.shortDescription)
                        .cookingTime(r.cookingTime)
                        .cookingTimeAll(r.cookingTimeAll)
                        .cookingPreperationTime(r.cookingPreperationTime)
                        .frontImage(r.frontImage)
                        .frontImageGallery(r.frontImageGallery).build())
                .collect(Collectors.toList());
    }

    private RecieptDTO toDTO(Reciept r) {
        return RecieptDTO.newBuilder()
                .title(r.title)
                .cookingTime(r.cookingTime)
                .cookingPreperationTime(r.cookingPreperationTime)
                .cookingTimeAll(r.cookingTimeAll)
                .id(r.id)
                .images(r.images)
                .content(r.content)
                .directions(r.directions)
                .ingredients(r.ingredients)
                .cookingTime(r.cookingTime)
                .frontImage(r.frontImage)
                .frontImageGallery(r.frontImageGallery)
                .build();
    }

    private File getDir() {
        File theDir = new File(Constant.imageFolder);
        if (!theDir.exists()) {
            theDir.mkdir();
        }
        return theDir;
    }
}