package cakery.core.reciept;

public class Ingredient {
	public final int amount;
	public final String type;
	public final String content;

	public Ingredient(int amount, String type, String content) {
		this.amount = amount;
		this.type = type;
		this.content = content;
	}

	public Ingredient() {
		amount = 0;
		type = null;
		content = null;
	}
}
