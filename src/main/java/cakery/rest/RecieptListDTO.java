package cakery.rest;

import java.time.LocalDateTime;

class RecieptListDTO {
    public final String id;
    public final String title;
    public final String shortDescription;
    public final Integer cookingTime;
    public final Integer cookingPreperationTime;
    public final Integer cookingTimeAll;
    public final String frontImage;
    public final String frontImageGallery;
    public final LocalDateTime createdDate;


    public RecieptListDTO() {
        title = null;
        shortDescription = null;
        cookingTime = null;
        frontImage = null;
        createdDate = null;
        id = null;
        cookingPreperationTime = null;
        frontImageGallery = null;
        cookingTimeAll = null;
    }

    private RecieptListDTO(Builder builder) {
        id = builder.id;
        title = builder.title;
        shortDescription = builder.shortDescription;
        cookingTime = builder.cookingTime;
        cookingPreperationTime = builder.cookingPreperationTime;
        cookingTimeAll = builder.cookingTimeAll;
        frontImage = builder.frontImage;
        frontImageGallery = builder.frontImageGallery;
        createdDate = builder.createdDate;
    }

    public static Builder newBuilder() {
        return new Builder();
    }


    public static final class Builder {
        private String id;
        private String title;
        private String shortDescription;
        private Integer cookingTime;
        private Integer cookingPreperationTime;
        private Integer cookingTimeAll;
        private String frontImage;
        private String frontImageGallery;
        private LocalDateTime createdDate;

        private Builder() {
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder shortDescription(String shortDescription) {
            this.shortDescription = shortDescription;
            return this;
        }

        public Builder cookingTime(Integer cookingTime) {
            this.cookingTime = cookingTime;
            return this;
        }

        public Builder cookingPreperationTime(Integer cookingPreperationTime) {
            this.cookingPreperationTime = cookingPreperationTime;
            return this;
        }

        public Builder cookingTimeAll(Integer cookingTimeAll) {
            this.cookingTimeAll = cookingTimeAll;
            return this;
        }

        public Builder frontImage(String frontImage) {
            this.frontImage = frontImage;
            return this;
        }

        public Builder frontImageGallery(String frontImageGallery) {
            this.frontImageGallery = frontImageGallery;
            return this;
        }

        public Builder createdDate(LocalDateTime createdDate) {
            this.createdDate = createdDate;
            return this;
        }

        public RecieptListDTO build() {
            return new RecieptListDTO(this);
        }
    }
}
