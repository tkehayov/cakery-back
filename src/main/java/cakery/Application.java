package cakery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@EnableMongoRepositories(basePackages = "cakery.persistence.reciept")
@SpringBootApplication(scanBasePackages = "cakery")
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}