package cakery;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigFilter {
	@Bean
	public FilterRegistrationBean userLoginRegistration() {
		FilterRegistrationBean registrationBean = new FilterRegistrationBean();
		registrationBean.addUrlPatterns("/admin/*");
		registrationBean.setFilter(new AdminAuthFilter());
		return registrationBean;
	}

}
