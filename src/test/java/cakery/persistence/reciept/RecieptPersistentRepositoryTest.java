package cakery.persistence.reciept;

import cakery.core.reciept.Image;
import cakery.core.reciept.Ingredient;
import cakery.core.reciept.Reciept;
import cakery.persistence.RecieptPersistentRepository;
import com.github.fakemongo.junit.FongoRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = FakeDBConfig.class)
public class RecieptPersistentRepositoryTest {
    @Rule
    public FongoRule fongo = new FongoRule();

    @Autowired
    private RecieptPersistentRepository recieptRepository;

    @Test
    public void addAndGetRecieptList() {
        Reciept reciept = Reciept.newBuilder().content("content").cookingTime(30).title("title")
                .ingredients(new ArrayList<Ingredient>() {
                    {
                        add(new Ingredient(2, "ch.ch", "flouver"));
                    }
                }).createdDate(LocalDateTime.parse("2017-01-04T20:11:41")).shortDescription("short description")
                .frontImage("frontImage").images(new ArrayList<Image>() {
                    {
                        add(new Image("image.jpg"));
                    }
                }).build();
        recieptRepository.add(reciept);

        List<Reciept> reciepts = recieptRepository.list(1);

        assertThat(reciepts.size(), is(1));
        assertThat(reciepts.get(0).content, is("content"));
        assertThat(reciepts.get(0).cookingTime, is(30));
        assertThat(reciepts.get(0).frontImage, is("frontImage"));
        assertThat(reciepts.get(0).images, is(reciept.images));
        assertThat(reciepts.get(0).title, is("title"));
        assertThat(reciepts.get(0).shortDescription, is("short description"));
        assertThat(reciepts.get(0).createdDate, is(LocalDateTime.parse("2017-01-04T20:11:41")));
        assertThat(reciepts.get(0).ingredients.get(0).amount, is(2));
        assertThat(reciepts.get(0).ingredients.get(0).type, is("ch.ch"));
        assertThat(reciepts.get(0).ingredients.get(0).content, is("flouver"));
    }

    @Test
    public void addTwoAndLimitOneReciept() {
        Reciept reciept = Reciept.newBuilder().content("content").cookingTime(30).title("title")
                .shortDescription("short description").frontImage("frontImage").images(new ArrayList<Image>() {
                    {
                        add(new Image("image.jpg"));
                    }
                }).build();
        recieptRepository.add(reciept);
        recieptRepository.add(reciept);

        List<Reciept> reciepts = recieptRepository.list(1);

        assertThat(reciepts.size(), is(1));
        assertThat(reciepts.get(0).content, is("content"));
        assertThat(reciepts.get(0).cookingTime, is(30));
        assertThat(reciepts.get(0).frontImage, is("frontImage"));
        assertThat(reciepts.get(0).images, is(reciept.images));
        assertThat(reciepts.get(0).title, is("title"));
        assertThat(reciepts.get(0).shortDescription, is("short description"));
    }

    @Test
    public void addGetReciept() {
        Reciept reciept = Reciept.newBuilder().content("content")
                .cookingTime(30)
                .title("title")
                .shortDescription("short description")
                .frontImage("frontImage")
                .category("pizza")
                .images(new ArrayList<Image>() {{
                    add(new Image("image.jpg"));
                }})
                .directions(new ArrayList<String>() {{
                    add("1 get clean dishe");
                    add("2 i said get CLEAN dish");
                    add("-1 eat the dish");
                }})
                .cookingPreperationTime(23)
                .cookingTimeAll(33)
                .build();
        recieptRepository.add(reciept);

        Reciept recieptRepo = recieptRepository.get(recieptRepository.list(1).get(0).id);

        assertThat(recieptRepo.content, is("content"));
        assertThat(recieptRepo.cookingTime, is(30));
        assertThat(recieptRepo.frontImage, is("frontImage"));
        assertThat(recieptRepo.images, is(recieptRepo.images));
        assertThat(recieptRepo.title, is("title"));
        assertThat(recieptRepo.shortDescription, is("short description"));
        assertThat(recieptRepo.cookingPreperationTime, is(23));
        assertThat(recieptRepo.cookingTimeAll, is(33));
        assertThat(recieptRepo.category, is("pizza"));
    }

    @Test
    public void delete(){
        Reciept reciept = Reciept.newBuilder()
                .content("content")
                .cookingTime(30).title("title")
                .build();
        recieptRepository.add(reciept);

        List<Reciept> reciepts = recieptRepository.list(1);

        recieptRepository.delete(reciepts.get(0).id);
        System.out.println("daaaaaa");
        assertThat(recieptRepository.list(1).size(), is(0));
    }
}
