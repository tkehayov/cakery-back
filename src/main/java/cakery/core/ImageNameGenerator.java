package cakery.core;

public interface ImageNameGenerator {

	String generate(String contentType);

}
