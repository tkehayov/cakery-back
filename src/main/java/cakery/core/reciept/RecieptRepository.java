package cakery.core.reciept;

import java.util.List;

public interface RecieptRepository {
	void add(Reciept reciept);

	List<Reciept> list(int limit);

	Reciept get(String id);

    void delete(String id);

	List<Reciept> category(String category);
}
