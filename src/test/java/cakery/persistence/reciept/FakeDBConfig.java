package cakery.persistence.reciept;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.github.fakemongo.Fongo;
import com.mongodb.Mongo;

import cakery.persistence.RecieptPersistentRepository;

@Configuration
@EnableMongoRepositories
@ComponentScan(basePackageClasses = { RecieptPersistentRepository.class })
public class FakeDBConfig extends AbstractMongoConfiguration {

	@Override
	public String getDatabaseName() {
		return "cakery-test";
	}

	@Override
	@Bean
	public Mongo mongo() throws Exception {
		return new Fongo("mongo-test").getMongo();
	}

}