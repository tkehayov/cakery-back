package cakery.persistence;

import cakery.core.reciept.Reciept;
import cakery.core.reciept.RecieptRepository;
import com.mongodb.BasicDBObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.CriteriaDefinition;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

import static cakery.core.reciept.Reciept.newBuilder;
import static com.mongodb.client.model.Filters.eq;

@Component
public class RecieptPersistentRepository implements RecieptRepository {

    @Autowired
    public MongoTemplate mongoTemplate;

    @Override
    public void add(Reciept reciept) {
        mongoTemplate.insert(toEntity(reciept));

    }

    @Override
    public List<Reciept> list(int limit) {
        Query qery = new Query().limit(limit).with(new Sort(Direction.DESC, "createdDate"));
        List<RecieptEntity> reciepts = mongoTemplate.find(qery, RecieptEntity.class);
        return toDo(reciepts);
    }

    @Override
    public Reciept get(String id) {
        RecieptEntity recieptEntity = mongoTemplate.findById(id, RecieptEntity.class);
        return toDo(recieptEntity);
    }

    @Override
    public void delete(String id) {
        RecieptEntity recieptEntity = mongoTemplate.findById(id, RecieptEntity.class);
        mongoTemplate.remove(recieptEntity);
    }

    @Override
    public List<Reciept> category(String category) {
        Query query = new Query().limit(10).with(new Sort(Direction.DESC, "createdDate"));
        query.addCriteria(Criteria.where("category").is(category));

        List<RecieptEntity> reciepts = mongoTemplate.find(query, RecieptEntity.class);

        return toDo(reciepts);
    }

    private RecieptEntity toEntity(Reciept reciept) {
        return RecieptEntity.newBuilder()
                .title(reciept.title)
                .content(reciept.content)
                .category(reciept.category)
                .cookingTime(reciept.cookingTime)
                .cookingTimeAll(reciept.cookingTimeAll)
                .cookingPreperationTime(reciept.cookingPreperationTime)
                .createdDate(reciept.createdDate)
                .ingredients(reciept.ingredients)
                .directions(reciept.directions)
                .shortDescription(reciept.shortDescription)
                .cookingTime(reciept.cookingTime)
                .frontImageGallery(reciept.frontImageGallery)
                .frontImage(reciept.frontImage).images(reciept.images)
                .build();
    }

    private List<Reciept> toDo(List<RecieptEntity> reciept) {
        return reciept.stream()
                .map(r -> newBuilder()
                        .title(r.title)
                        .category(r.category)
                        .content(r.content)
                        .cookingTime(r.cookingTime)
                        .cookingPreperationTime(r.cookingPreperationTime)
                        .cookingTimeAll(r.cookingTimeAll)
                        .id(r.id)
                        .ingredients(r.ingredients)
                        .createdDate(r.createdDate)
                        .shortDescription(r.shortDescription)
                        .directions(r.directions)
                        .frontImage(r.frontImage)
                        .images(r.images).build())
                .collect(Collectors.toList());
    }

    private Reciept toDo(RecieptEntity r) {
        return newBuilder()
                .title(r.title)
                .category(r.category)
                .content(r.content)
                .cookingTime(r.cookingTime)
                .cookingPreperationTime(r.cookingPreperationTime)
                .cookingTimeAll(r.cookingTimeAll)
                .id(r.id)
                .directions(r.directions)
                .ingredients(r.ingredients)
                .createdDate(r.createdDate)
                .frontImageGallery(r.frontImageGallery)
                .shortDescription(r.shortDescription)
                .frontImage(r.frontImage)
                .images(r.images)
                .build();
    }
}
