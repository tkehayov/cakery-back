package cakery.persistence;

import cakery.core.reciept.Image;
import cakery.core.reciept.Ingredient;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;

@Document(collection = "reciept")
class RecieptEntity {
    @Id
    public final String id;
    public final String title;
    public final String content;
    public final String shortDescription;
    public final Integer cookingTime;
    public final Integer cookingTimeAll;
    public final Integer cookingPreperationTime;
    public final String frontImage;
    public final String category;
    public final String frontImageGallery;
    public final List<Image> images;
    public final LocalDateTime createdDate;
    public final List<Ingredient> ingredients;
    public final List<String> directions;

    public RecieptEntity() {
        id = null;
        title = null;
        content = null;
        shortDescription = null;
        cookingTime = null;
        cookingTimeAll = null;
        cookingPreperationTime = null;
        frontImage = null;
        category = null;
        frontImageGallery = null;
        images = null;
        createdDate = null;
        ingredients = null;
        directions = null;
    }

    private RecieptEntity(Builder builder) {
        id = builder.id;
        title = builder.title;
        content = builder.content;
        shortDescription = builder.shortDescription;
        cookingTime = builder.cookingTime;
        cookingTimeAll = builder.cookingTimeAll;
        cookingPreperationTime = builder.cookingPreperationTime;
        frontImage = builder.frontImage;
        category = builder.category;
        frontImageGallery = builder.frontImageGallery;
        images = builder.images;
        createdDate = builder.createdDate;
        ingredients = builder.ingredients;
        directions = builder.directions;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static final class Builder {
        private String id;
        private String title;
        private String content;
        private String shortDescription;
        private Integer cookingTime;
        private Integer cookingTimeAll;
        private Integer cookingPreperationTime;
        private String frontImage;
        private String category;
        private String frontImageGallery;
        private List<Image> images;
        private LocalDateTime createdDate;
        private List<Ingredient> ingredients;
        private List<String> directions;

        private Builder() {
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder content(String content) {
            this.content = content;
            return this;
        }

        public Builder shortDescription(String shortDescription) {
            this.shortDescription = shortDescription;
            return this;
        }

        public Builder cookingTime(Integer cookingTime) {
            this.cookingTime = cookingTime;
            return this;
        }

        public Builder cookingTimeAll(Integer cookingTimeAll) {
            this.cookingTimeAll = cookingTimeAll;
            return this;
        }

        public Builder cookingPreperationTime(Integer cookingPreperationTime) {
            this.cookingPreperationTime = cookingPreperationTime;
            return this;
        }

        public Builder frontImage(String frontImage) {
            this.frontImage = frontImage;
            return this;
        }

        public Builder category(String category) {
            this.category = category;
            return this;
        }

        public Builder frontImageGallery(String frontImageGallery) {
            this.frontImageGallery = frontImageGallery;
            return this;
        }

        public Builder images(List<Image> images) {
            this.images = images;
            return this;
        }

        public Builder createdDate(LocalDateTime createdDate) {
            this.createdDate = createdDate;
            return this;
        }

        public Builder ingredients(List<Ingredient> ingredients) {
            this.ingredients = ingredients;
            return this;
        }

        public Builder directions(List<String> directions) {
            this.directions = directions;
            return this;
        }

        public RecieptEntity build() {
            return new RecieptEntity(this);
        }
    }


//    private RecieptEntity(Builder builder) {
//        id = builder.id;
//        title = builder.title;
//        content = builder.content;
//        category = builder.category;
//        shortDescription = builder.shortDescription;
//        cookingTime = builder.cookingTime;
//        cookingTimeAll = builder.cookingTimeAll;
//        cookingPreperationTime = builder.cookingPreperationTime;
//        frontImage = builder.frontImage;
//        frontImageGallery = builder.frontImageGallery;
//        images = builder.images;
//        createdDate = builder.createdDate;
//        ingredients = builder.ingredients;
//        directions = builder.directions;
//    }
//
//    public static Builder newBuilder() {
//        return new Builder();
//    }
//
//
//    public static final class Builder {
//        private String id;
//        private String title;
//        private String content;
//        private String category;
//        private String shortDescription;
//        private Integer cookingTime;
//        private Integer cookingTimeAll;
//        private Integer cookingPreperationTime;
//        private String frontImage;
//        private String frontImageGallery;
//        private List<Image> images;
//        private LocalDateTime createdDate;
//        private List<Ingredient> ingredients;
//        private List<String> directions;
//
//        private Builder() {
//        }
//
//        public Builder id(String id) {
//            this.id = id;
//            return this;
//        }
//
//        public Builder title(String title) {
//            this.title = title;
//            return this;
//        }
//
//        public Builder content(String content) {
//            this.content = content;
//            return this;
//        }
//
//        public Builder category(String category) {
//            this.category = category;
//            return this;
//        }
//
//        public Builder shortDescription(String shortDescription) {
//            this.shortDescription = shortDescription;
//            return this;
//        }
//
//        public Builder cookingTime(Integer cookingTime) {
//            this.cookingTime = cookingTime;
//            return this;
//        }
//
//        public Builder cookingTimeAll(Integer cookingTimeAll) {
//            this.cookingTimeAll = cookingTimeAll;
//            return this;
//        }
//
//        public Builder cookingPreperationTime(Integer cookingPreperationTime) {
//            this.cookingPreperationTime = cookingPreperationTime;
//            return this;
//        }
//
//        public Builder frontImage(String frontImage) {
//            this.frontImage = frontImage;
//            return this;
//        }
//
//        public Builder frontImageGallery(String frontImageGallery) {
//            this.frontImageGallery = frontImageGallery;
//            return this;
//        }
//
//        public Builder images(List<Image> images) {
//            this.images = images;
//            return this;
//        }
//
//        public Builder createdDate(LocalDateTime createdDate) {
//            this.createdDate = createdDate;
//            return this;
//        }
//
//        public Builder ingredients(List<Ingredient> ingredients) {
//            this.ingredients = ingredients;
//            return this;
//        }
//
//        public Builder directions(List<String> directions) {
//            this.directions = directions;
//            return this;
//        }
//
//        public RecieptEntity build() {
//            return new RecieptEntity(this);
//        }
//    }
}
