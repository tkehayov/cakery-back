package cakery.rest;

import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.SSLEngineResult.Status;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;

import cakery.core.reciept.Image;
import cakery.core.reciept.Reciept;
import cakery.core.reciept.RecieptRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class RecieptControllerTest {
	@Autowired
	private MockMvc mockMvc;
	RecieptRepository mockedList = mock(RecieptRepository.class);

//	@Test
//	public void add() throws Exception {
//		Reciept reciept = Reciept.builder().title("title").content("content").cookingTime(23).frontImage("fron.jpg")
//				.createdDate(LocalDateTime.parse("2017-01-04T20:11:41")).shortDescription("short descr")
//				.images(new ArrayList<Image>() {
//					{
//						add(new Image("image.jpg"));
//					}
//				}).build();
//
//		String content = new ObjectMapper().writeValueAsString(reciept);
//
//		mockMvc.perform(post("/reciepts").contentType(MediaType.APPLICATION_JSON).content(content))
//				.andExpect(status().isOk());
//	}

	@Test
	public void list() throws Exception {
		List<Reciept> reciepts = new ArrayList() {
			{
				add(Reciept.newBuilder().title("title").content("content").cookingTime(23).frontImage("fron.jpg")
						.shortDescription("short descr").images(new ArrayList<Image>() {
							{
								add(new Image("image.jpg"));
							}
						}).build());
			}
		};

		String content = new ObjectMapper().writeValueAsString(reciepts);

		mockMvc.perform(get("/reciepts?limit=10").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}
}
