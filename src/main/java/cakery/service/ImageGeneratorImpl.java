package cakery.service;

import java.util.UUID;

import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Component;

import cakery.core.ImageNameGenerator;

@Component
public class ImageGeneratorImpl implements ImageNameGenerator {

	@Override
	public String generate(String filename) {
		UUID random = UUID.randomUUID();
		String fileExtension = FilenameUtils.getExtension(filename);

		return random + "." + fileExtension;
	}

}
