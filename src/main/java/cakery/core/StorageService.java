package cakery.core;

import java.io.InputStream;

public interface StorageService {

	void save(InputStream inputStream, String destination);
}
